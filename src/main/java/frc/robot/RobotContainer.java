/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.DriveJoystickMappings;
import frc.robot.commands.BottomConveyor.BottomConveyorIn;
import frc.robot.commands.ColorWheel.RotateToColor;
import frc.robot.commands.ColorWheel.RotateWheel;
import frc.robot.commands.ColorWheel.WhiteLineStop;
import frc.robot.commands.CommandGroups.BothConveyorsAndShoot;
import frc.robot.commands.CommandGroups.IntakeAndBottom;
import frc.robot.commands.CommandGroups.TimedBothConveyors;
import frc.robot.commands.DriveTrain.DefaultDrive;
import frc.robot.commands.DriveTrain.DriveStraight;
import frc.robot.commands.DriveTrain.RotateToAngle;
import frc.robot.commands.Elevator.ElevatorToBottom;
import frc.robot.commands.Elevator.ElevatorToTop;
import frc.robot.commands.Intake.IntakeIn;
import frc.robot.commands.Intake.IntakeOut;
import frc.robot.commands.Test.RunMotorTest;
import frc.robot.commands.Shooter.AutonomousShoot;
import frc.robot.commands.Shooter.ShootCommand;
import frc.robot.commands.TopConveyor.TopConveyorIn;
import frc.robot.commands.TopConveyor.TopConveyorOut;
import frc.robot.commands.Vision.ToggleVisionLight;
import frc.robot.subsystems.BottomConveyor;
import frc.robot.subsystems.ColorWheel;
import frc.robot.subsystems.MotorTest;
import frc.robot.subsystems.DriveTrain;
import frc.robot.commands.BottomConveyor.BottomConveyorIn;
import frc.robot.commands.BottomConveyor.BottomConveyorOut;
import frc.robot.subsystems.Elevator;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.TopConveyor;
import frc.robot.subsystems.Vision;
import frc.robot.subsystems.ColorWheel;
import frc.robot.commands.DriveTrain.DriveStraight;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */

public class RobotContainer {
    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public Joystick m_driveJoystick = new Joystick(0);
    private Joystick m_buttonJoystick = new Joystick(1);
    // Subsystems
    private Elevator m_elevator = new Elevator();
    private Shooter m_shooter = new Shooter();
    private Intake m_intake = new Intake();
    private ColorWheel m_ColorWheel = new ColorWheel();
    private TopConveyor m_topConveyor = new TopConveyor();
    // private Intake m_intake = new Intake();
    // private ColorWheel m_colorWheel = new ColorWheel();
    private Vision m_vision = new Vision();
    // TODO: make below private for final code. currently its public so gyro can be
    // reset on teleop init(see Robot.java teleop init)
    public DriveTrain m_driveTrain = new DriveTrain();
    private BottomConveyor m_bottomConveyor = new BottomConveyor();
    private Intake m_test = new Intake(8);
    private Intake m_test2 = new Intake(10);
    private Intake m_ColorWheel2 = new Intake(13);

    public RobotContainer() {
        // Configure the button bindings
        m_driveTrain.setDefaultCommand(new DefaultDrive(m_driveTrain, () -> -m_driveJoystick.getY(),
                m_driveJoystick::getX, () -> m_driveJoystick.getRawButton(DriveJoystickMappings.quickTurn)));

        configureButtonBindings();
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be
     * created by instantiating a {@link GenericHID} or one of its subclasses
     * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
     * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        // Elevator buttons
        new JoystickButton(m_buttonJoystick, 5).whenHeld(new IntakeOut(m_intake));
        new JoystickButton(m_buttonJoystick, 6).whenHeld(new IntakeIn(m_intake));
        new JoystickButton(m_buttonJoystick, 2).whenHeld(new BottomConveyorIn(m_bottomConveyor));
        new JoystickButton(m_buttonJoystick, 4).whenHeld(new BottomConveyorOut(m_bottomConveyor));
        new JoystickButton(m_buttonJoystick, 1).whenHeld(new TopConveyorIn(m_topConveyor));
        new JoystickButton(m_buttonJoystick, 3).whenHeld(new TopConveyorOut(m_topConveyor));

        /// new JoystickButton(m_buttonJoystick, 2)
        // .whenHeld(new Both(m_shooter, m_topConveyor, m_bottomConveyor, 500));
        new JoystickButton(m_driveJoystick, 1).whenHeld(new ShootCommand(m_shooter));
        /*
         * JoystickButton(m_driveJoystick, 2).whenHeld(new IntakeIn(m_intake)); new
         * JoystickButton(m_driveJoystick, 4).whenHeld(new
         * BottomConveyorIn(m_bottomConveyor)); // new JoystickButton(m_driveJoystick,
         * 5).whenHeld(new // TopConveyorIn(m_topConveyor));
         * 
         * new JoystickButton(m_driveJoystick, 6).whenHeld(new
         * ElevatorToTop(m_elevator));
         * 
         * // new JoystickButton(m_driveJoystick, 3).whenHeld(new
         * IntakeIn(m_ColorWheel2));
         */
        new JoystickButton(m_driveJoystick, 5).whenHeld(new IntakeIn(m_test2));
        new JoystickButton(m_driveJoystick, 3).whenHeld(new IntakeOut(m_test2));
        new JoystickButton(m_driveJoystick, 5).whenHeld(new IntakeIn(m_test));
        new JoystickButton(m_driveJoystick, 3).whenHeld(new IntakeOut(m_test));

        /*
         * // BottomConveyorIn(m_bottomConveyor, isShootable())); // new
         * JoystickButton(m_driveJoystick, 1).whenPressed(command) // new
         * JoystickButton(m_driveJoystick, 1).whileHeld(new //
         * BottomConveyorIn(m_bottomConveyor)); // new JoystickButton(m_driveJoystick,
         * 2).whileHeld(new // TopConveyorOut(m_topConveyor));
         * 
         * // new JoystickButton(m_driveJoystick, 4).whileHeld(new //
         * IntakeAndBottom(m_intake, m_bottomConveyor, false)); // new
         * JoystickButton(m_driveJoystick, 3).whileHeld(new // IntakeAndBottom(m_intake,
         * m_bottomConveyor, true));
         * 
         * // new JoystickButton(m_driveJoystick, 1).whenHeld(new //
         * TimedBothConveyors(m_topConveyor, m_bottomConveyor)); new
         * JoystickButton(m_driveJoystick, 6).whenPressed(new
         * ToggleVisionLight(m_vision));
         */
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        return new DriveStraight(0.0049, 0, m_driveTrain);
    }
}
