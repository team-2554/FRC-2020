package frc.robot.commands.Test;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.MotorTest;

public class RunMotorTest extends CommandBase {
  private final MotorTest testingMotor;

  public RunMotorTest(final MotorTest t_motor) {
    testingMotor = t_motor;
    addRequirements(testingMotor);
  }

  public void execute() {
    testingMotor.start(false);
  }

  public void end() {
    testingMotor.stop();
  }

  public boolean isFinished() {
    return false;
  }

}