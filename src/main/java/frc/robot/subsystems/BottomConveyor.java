/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ConveyorConstants;

public class BottomConveyor extends SubsystemBase {
  /**
   * Creates a new BottomConveyor.
   */
  private final WPI_VictorSPX m_bottomConveyor = new WPI_VictorSPX(ConveyorConstants.bottomConveyorPort);

  public BottomConveyor() {

    m_bottomConveyor.enableVoltageCompensation(true);
    m_bottomConveyor.configVoltageCompSaturation(ConveyorConstants.topConveyorVoltage);
  }

  public void conveyorIn() {
    m_bottomConveyor.set(-1);
  }

  public void conveyorOut() {
    m_bottomConveyor.set(1);
  }

  public void stopConveyor() {
    m_bottomConveyor.stopMotor();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
