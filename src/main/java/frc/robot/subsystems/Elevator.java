/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.ElevatorConstants;

public class Elevator extends SubsystemBase {
  // Initializes both motors
  private final WPI_VictorSPX elevatorMotor1 = new WPI_VictorSPX(ElevatorConstants.motorPort1);
  private final WPI_VictorSPX elevatorMotor2 = new WPI_VictorSPX(ElevatorConstants.motorPort2);

  // Binds both motors to a SpeedControllerGroup
  private final SpeedControllerGroup elevatorMotors = new SpeedControllerGroup(elevatorMotor2);
  // private final SpeedControllerGroup elevatorMotors = new
  // SpeedControllerGroup(elevatorMotor1, elevatorMotor2);

  // Idk what this does its never used
  private final DigitalInput bottomSwitch = new DigitalInput(ElevatorConstants.bottomSwitch);
  private final DigitalInput topSwitch = new DigitalInput(ElevatorConstants.topSwitch);

  public Elevator() {
    // Sets up volage for motor1
    elevatorMotor1.enableVoltageCompensation(true);
    elevatorMotor1.configVoltageCompSaturation(ElevatorConstants.elevatorVoltage);

    // Sets up volage for motor2
    elevatorMotor2.enableVoltageCompensation(true);
    elevatorMotor2.configVoltageCompSaturation(ElevatorConstants.elevatorVoltage);

  }

  public void holdPower() {
    // idk what this does either
    elevatorMotors.set(Constants.ElevatorConstants.holdPowerConstant);
  }

  public void stopElevator() {
    // Stops motor and holds power
    elevatorMotor2.set(ControlMode.PercentOutput, 0);
    // holdPower();
  }

  public void forceStopElevator() {
    // Force stops motor
    elevatorMotor2.set(ControlMode.PercentOutput, 0);
  }

  public void goUp() {
    // Sets motor group speed to 1
    elevatorMotor2.set(ControlMode.PercentOutput, 1);
  }

  public void goDown() {
    // Sets motor group speed to -1
    elevatorMotor2.set(ControlMode.PercentOutput, -1);
  }

  public boolean atTop() {
    return topSwitch.get();
  }

  public boolean atBottom() {
    return bottomSwitch.get();
  }
}
